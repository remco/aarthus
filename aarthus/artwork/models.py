# -*- coding: utf-8 -*-
import datetime as dt

from aarthus.database import (
    Column,
    db,
    Model,
    ReferenceCol,
    relationship,
    SurrogatePK,
)


class Artwork(SurrogatePK, db.Model):
    __tablename__ = 'artwork'

    name = Column(db.String(80), unique=True, nullable=False)
    artist = Column(db.String(80), unique=False, nullable=False)
    year = Column(db.String(4), unique=False, nullable=True)
    type = Column(db.String(80), unique=False, nullable=True)
    attributed = Column(db.Boolean, unique=False, nullable=False, default=False)
    size1 = Column(db.Integer, unique=False, nullable=True)
    size2 = Column(db.Integer, unique=False, nullable=True)
    size3 = Column(db.Integer, unique=False, nullable=True)
    color = Column(db.String(7), unique=False, nullable=True)
    image = Column(db.String(255), unique=False, nullable=True)

    description = Column(db.Text, unique=False, nullable=True)
    literature = Column(db.Text, unique=False, nullable=True)

    def __init__(self, name, **kwargs):
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<Artwork({name})>'.format(name=self.name)

# -*- coding: utf-8 -*-
import os

from flask import Blueprint, request, render_template, url_for, redirect, send_from_directory
from werkzeug.utils import secure_filename


blueprint = Blueprint('artwork', __name__, static_folder="../static")

@blueprint.route('/artwork/')
def list():
    return render_template("artwork/list.html")

@blueprint.route('/upload', methods=['POST'])
def upload_file():
    file = request.files['file']
    if file:
        filename = secure_filename(request.form['flowIdentifier'])
        # XXX Get UPLOAD folder from config
        file.save(os.path.join('/tmp/', filename))
        return redirect(url_for('artwork.uploaded_file',
                                filename=filename))

@blueprint.route('/uploads/<filename>')
def uploaded_file(filename):
    # XXX Get UPLOAD folder from config
    return send_from_directory('/tmp/',
                               filename)
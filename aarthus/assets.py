# -*- coding: utf-8 -*-
from flask.ext.assets import Bundle, Environment

css = Bundle(
    "libs/bootstrap/dist/css/bootstrap.css",
    "libs/angular-xeditable/dist/css/xeditable.css",
    "css/style.css",
    filters="cssmin",
    output="public/css/common.css"
)

js = Bundle(
    "libs/angular/angular.js",
    "libs/lodash/dist/lodash.js",
    "libs/restangular/dist/restangular.js",
    "libs/angular-xeditable/dist/js/xeditable.js",
    "libs/angular-bootstrap/ui-bootstrap.js",
    "libs/angular-bootstrap/ui-bootstrap-tpls.js",
    "libs/flow.js/dist/flow.js",
    "libs/ng-flow/dist/ng-flow.js",
    filters='jsmin',
    output="public/js/common.js"
)

assets = Environment()

assets.register("js_all", js)
assets.register("css_all", css)
/*global angular */
'use strict';

var artwork_app = angular.module(
    'artwork', ['restangular', 'xeditable', 'ui.bootstrap', 'flow']);

// Restangular config
artwork_app.config(function(RestangularProvider) {
    RestangularProvider.setBaseUrl('/api');
    // REST api returns nested structure, handle this correctly
    RestangularProvider.addResponseInterceptor(
        function(data, operation, what, url, response, deferred) {
            var extractedData;
            if (operation === "getList") {
                extractedData = data.objects;
            } else {
                extractedData = data;
            }

            return extractedData;
        }
    );
})
.run(function(editableOptions) {
    // BS 3 theme for xeditable angular
    editableOptions.theme = 'bs3';
});

// Angular flow config
artwork_app.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
        target: '/upload',
        permanentErrors: [500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 1
    };
    flowFactoryProvider.on('catchAll', function (event) {
        console.log('catchAll', arguments);
    });
}]);

artwork_app.controller('IndexCtrl', function($scope, $modal, Restangular) {
    var baseArtworks = Restangular.all('artwork');

    $scope.artworks = baseArtworks.getList().$object;
    $scope.types = [
        {value: 'sculpture', text: 'sculpture'},
        {value: 'video', text: 'video'}
    ];

    $scope.new = function() {
        // XXX: Duplication of model layout
        var new_artwork = {
            "artist": "",
            "attributed": false,
            "description": "",
            "literature": "",
            "name": "",
            "size1": 0,
            "size2": 0,
            "size3": 0,
            "type": "",
            "year": ""
        };
        // Put the restangularized element in the scope
        $scope.inserted = Restangular.restangularizeElement(null, new_artwork, 'artwork');
        $scope.artworks.push($scope.inserted);
    };

    $scope.save = function(artwork) {
        // Save and use the (computed) remote return value to update the local id
        artwork.save().then(function(saved_artwork) {
            artwork.id = saved_artwork.id
        })
    };

    $scope.delete = function(artwork) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteArtwork.html',
            controller: ModalInstanceCtrl,
            resolve: {
                artworks_to_delete: function () {
                    // XXX for now only allow to delete one
                    return [artwork];
                }
            }
        });

        modalInstance.result.then(function () {
            // Delete item both locally and in the api
            // XXX: only delete when call successful
            var index = _.indexOf($scope.artworks, artwork);
            // Remove artwork without making array copy, so angular does a nice update
            $scope.artworks.splice(index, 1);
            artwork.remove();
        }, function () {
            // Cancel was called, ignore
        });
    };

});

var ModalInstanceCtrl = function ($scope, $modalInstance, artworks_to_delete) {
      $scope.artworks_to_delete = artworks_to_delete;

      $scope.ok = function () {
        $modalInstance.close();
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
};
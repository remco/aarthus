# -*- coding: utf-8 -*-
from flask import Blueprint, url_for, redirect, render_template

blueprint = Blueprint('main', __name__, static_folder="../static")

@blueprint.route('/')
def home():
    return redirect(url_for('artwork.list'))

@blueprint.route('/about/')
def about():
    return render_template("main/about.html")

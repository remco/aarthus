#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import subprocess
from flask.ext.script import Manager, Shell, Server
from flask.ext.migrate import MigrateCommand

from aarthus.app import create_app
from aarthus.artwork.models import Artwork
from aarthus.settings import DevConfig, ProdConfig
from aarthus.database import db

if os.environ.get("AARTHUS_ENV") == 'prod':
    app = create_app(ProdConfig)
else:
    app = create_app(DevConfig)

manager = Manager(app)
TEST_CMD = "py.test tests"

def _make_context():
    """Return context dict for a shell session so you can access
    app, db, and the User model by default.
    """
    return {'app': app, 'db': db}

@manager.command
def test():
    """Run the tests."""
    status = subprocess.call(TEST_CMD, shell=True)
    sys.exit(status)


# Allow cross origin requests for testing
def add_cors_header(response):
    response.headers['Access-Control-Allow-Origin'] = 'http://127.0.0.1:9000'
    response.headers['Access-Control-Allow-Methods'] = 'HEAD, GET, POST, PATCH, PUT, OPTIONS, DELETE'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
    response.headers['Access-Control-Allow-Credentials'] = 'true'

    return response
app.after_request(add_cors_header)

# XXX: this should go into separate api module in the artwork package
from aarthus.extensions import api_manager
api_manager.create_api(Artwork, methods=['GET', 'PUT', 'POST', 'DELETE'])

manager.add_command('server', Server())
manager.add_command('shell', Shell(make_context=_make_context))
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
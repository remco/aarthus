Installation
============

On Mac using homebrew:

```bash
$ brew install caskroom/cask/brew-cask
$ brew cask install virtualbox vagrant
$ brew install docker python
$ curl https://raw.githubusercontent.com/noplay/docker-osx/0.11.1/docker-osx > /usr/local/bin/docker-osx
$ chmod +x /usr/local/bin/docker-osx
$ pip install fig
$ docker-osx start
$ `docker-osx env`  # this export in .profile of a kind
$ fig up
```

You should now be able to see the app at: http://localdocker:5000

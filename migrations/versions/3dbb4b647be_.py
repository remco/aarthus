"""empty message

Revision ID: 3dbb4b647be
Revises: 477aed151c3
Create Date: 2014-05-27 12:40:00.793217

"""

# revision identifiers, used by Alembic.
revision = '3dbb4b647be'
down_revision = '477aed151c3'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('artwork', 'gallery_id')
    op.drop_table('gallery')
    op.add_column('artwork', sa.Column('attributed', sa.Boolean(), nullable=False, server_default='0'))
    op.add_column('artwork', sa.Column('description', sa.Text(), nullable=True))
    op.add_column('artwork', sa.Column('literature', sa.Text(), nullable=True))
    op.add_column('artwork', sa.Column('size1', sa.Integer(), nullable=True))
    op.add_column('artwork', sa.Column('size2', sa.Integer(), nullable=True))
    op.add_column('artwork', sa.Column('size3', sa.Integer(), nullable=True))
    op.add_column('artwork', sa.Column('type', sa.String(length=80), nullable=True))
    op.add_column('artwork', sa.Column('year', sa.String(length=4), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('gallery',
                    sa.Column('id', sa.INTEGER(), server_default="nextval('gallery_id_seq'::regclass)", nullable=False),
                    sa.Column('name', sa.VARCHAR(length=80), autoincrement=False, nullable=False),
                    sa.PrimaryKeyConstraint('id', name='gallery_pkey')
    )
    op.add_column('artwork', sa.Column('gallery_id', sa.INTEGER(), autoincrement=False, nullable=True))
    op.drop_column('artwork', 'year')
    op.drop_column('artwork', 'type')
    op.drop_column('artwork', 'size3')
    op.drop_column('artwork', 'size2')
    op.drop_column('artwork', 'size1')
    op.drop_column('artwork', 'literature')
    op.drop_column('artwork', 'description')
    op.drop_column('artwork', 'attributed')
    ### end Alembic commands ###

FROM stackbrew/ubuntu:14.04

MAINTAINER Remco Wendt "remco.wendt@gmail.com"

RUN apt-get update -qq && apt-get install -y python3.4 python3.4-dev python3-pip postgresql-client-9.3 python3-psycopg2

ADD requirements/prod.txt /tmp/
ADD requirements/dev.txt /tmp/
RUN pip3 install -r /tmp/dev.txt
WORKDIR /aarthus
